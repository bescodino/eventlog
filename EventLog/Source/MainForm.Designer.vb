﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnBaixarEventos = New System.Windows.Forms.Button()
        Me.btnPower = New System.Windows.Forms.Button()
        Me.btnSincronizar = New System.Windows.Forms.Button()
        Me.cbxPorta = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnBaixarEventos)
        Me.Panel1.Controls.Add(Me.btnPower)
        Me.Panel1.Controls.Add(Me.btnSincronizar)
        Me.Panel1.Controls.Add(Me.cbxPorta)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(8, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Padding = New System.Windows.Forms.Padding(10, 40, 10, 40)
        Me.Panel1.Size = New System.Drawing.Size(878, 462)
        Me.Panel1.TabIndex = 1
        '
        'btnBaixarEventos
        '
        Me.btnBaixarEventos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBaixarEventos.Enabled = False
        Me.btnBaixarEventos.Location = New System.Drawing.Point(755, 427)
        Me.btnBaixarEventos.Name = "btnBaixarEventos"
        Me.btnBaixarEventos.Size = New System.Drawing.Size(110, 23)
        Me.btnBaixarEventos.TabIndex = 5
        Me.btnBaixarEventos.Text = "Baixar Eventos"
        Me.btnBaixarEventos.UseVisualStyleBackColor = True
        '
        'btnPower
        '
        Me.btnPower.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPower.Location = New System.Drawing.Point(632, 427)
        Me.btnPower.Name = "btnPower"
        Me.btnPower.Size = New System.Drawing.Size(95, 23)
        Me.btnPower.TabIndex = 4
        Me.btnPower.Text = "Conectar"
        Me.btnPower.UseVisualStyleBackColor = True
        '
        'btnSincronizar
        '
        Me.btnSincronizar.Location = New System.Drawing.Point(166, 9)
        Me.btnSincronizar.Name = "btnSincronizar"
        Me.btnSincronizar.Size = New System.Drawing.Size(99, 23)
        Me.btnSincronizar.TabIndex = 3
        Me.btnSincronizar.Text = "Sincronizar RTC"
        Me.btnSincronizar.UseVisualStyleBackColor = True
        '
        'cbxPorta
        '
        Me.cbxPorta.FormattingEnabled = True
        Me.cbxPorta.Location = New System.Drawing.Point(57, 11)
        Me.cbxPorta.Name = "cbxPorta"
        Me.cbxPorta.Size = New System.Drawing.Size(87, 21)
        Me.cbxPorta.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Porta :"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 478)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Padding = New System.Windows.Forms.Padding(8)
        Me.Text = "EventLog"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MultiLineListBox1 As EventLog.MultiLineListBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnPower As Button
    Friend WithEvents btnSincronizar As Button
    Friend WithEvents cbxPorta As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnBaixarEventos As Button
End Class

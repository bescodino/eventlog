﻿Public Class ThreadManager

    Private Shared mLock As New Object
    Private Shared mCloseRequested As Boolean


    Public Shared Property CloseRequested As Boolean
        Get
            SyncLock mLock
                Return mCloseRequested
            End SyncLock
        End Get
        Set(pValue As Boolean)
            SyncLock mLock
                mCloseRequested = pValue
            End SyncLock
        End Set
    End Property


    Public Function WaitForNextCycle() As Boolean

        Dim cycleTime As Integer
        Dim waitTime As Integer

        cycleTime = 1000

        While (waitTime < cycleTime)
            If (CloseRequested) Then Return False
            Threading.Thread.Sleep(1000)
            waitTime += 1000
        End While

        Return True

    End Function

End Class

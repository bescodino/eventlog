﻿Public Class MultiLineListBox
    Inherits System.Windows.Forms.ListBox


    Public Sub New()
        DrawMode = DrawMode.OwnerDrawVariable
        ScrollAlwaysVisible = True
    End Sub

    Protected Overrides Sub OnMeasureItem(e As MeasureItemEventArgs)

        If (Site IsNot Nothing) Then
            Return
        End If

        If (e.Index > -1) Then
            Dim s As String = Items(e.Index).ToString()
            Dim sf As SizeF = e.Graphics.MeasureString(s, Font, Width)
            e.ItemHeight = CInt(sf.Height)
            e.ItemWidth = Width
        End If

    End Sub


    Public Sub AddItem(pItem As String)

        Items.Add(pItem)

        If (Items.Count > 300) Then
            Items.RemoveAt(0)
        End If

        Refresh()

    End Sub


    Protected Overrides Sub OnDrawItem(e As DrawItemEventArgs)

        If (Site IsNot Nothing) Then
            Return
        End If

        If (e.Index > -1) Then
            Dim s As String = Items(e.Index).ToString()

            If ((e.State And DrawItemState.Focus) = 0) Then
                e.Graphics.FillRectangle(New SolidBrush(SystemColors.Window), e.Bounds)
                e.Graphics.DrawString(s, Font, New SolidBrush(SystemColors.WindowText), e.Bounds)
            Else
                e.Graphics.FillRectangle(New SolidBrush(SystemColors.Highlight), e.Bounds)
                e.Graphics.DrawString(s, Font, New SolidBrush(SystemColors.HighlightText), e.Bounds)
            End If

        End If

    End Sub

End Class

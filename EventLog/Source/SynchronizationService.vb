﻿Public Class SynchronizationService

    Public Shared Sub Run()

        Try
            If (Globals.CommonPort.IsOpen() = False) Then
                Globals.CommonPort.Open()
            End If

            Dim currentTime As DateTime = DateTime.Now

            Dim day As Byte = currentTime.Day
            Dim month As Byte = currentTime.Month
            Dim currentYear = currentTime.Year - 2000
            Dim year As Byte = currentYear
            Dim hour As Byte = currentTime.Hour
            Dim minute As Byte = currentTime.Minute
            Dim seconds As Byte = currentTime.Second

            Dim dayOfWeek As Byte = currentTime.DayOfWeek + 1

            Logger.Instance.Info("Sincronizando o tempo do hardware...")

            ' Domingo igual a 1
            If (dayOfWeek = 8) Then
                dayOfWeek = 1
            End If

            Dim defaultDelay As Integer = 1000

            Send("sec.", seconds, defaultDelay)
            Send("min.", minute, defaultDelay)
            Send("hor.", hour, defaultDelay)
            Send("dow.", dayOfWeek, defaultDelay)
            Send("day.", day, defaultDelay)
            Send("mth.", month, defaultDelay)
            Send("yea.", year, defaultDelay)

            Logger.Instance.Info("Sincronizado com sucesso.")

        Catch ex As TimeoutException
            If (ThreadManager.CloseRequested) Then Return
            Logger.Instance.Error("Erro de timeout.")
        Catch ex As Exception
            If (ThreadManager.CloseRequested) Then Return
            Logger.Instance.Error(ex.Message.Trim())
        Finally
            If Globals.CommonPort IsNot Nothing Then Globals.CommonPort.Close()
        End Try

    End Sub



    Public Shared Sub Send(pCommand As String, pValue As Byte, pDelay As Integer)

        Dim byteArr As New List(Of Byte)

        byteArr.AddRange(System.Text.Encoding.ASCII.GetBytes(pCommand))
        byteArr.Add(pValue)

        Globals.CommonPort.Write(byteArr.ToArray(), 0, byteArr.Count)

        System.Threading.Thread.Sleep(pDelay)

    End Sub

End Class


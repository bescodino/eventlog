﻿Imports Monitoring

Public Class Main

 <STAThread()>
    Shared Sub Main()

        Dim applicationForm As MainForm

        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)

        applicationForm = New MainForm
        
        Logger.Setup(applicationForm)
        
        Logger.Instance.Info(String.Format("Application started - {0} ({1})", Reflection.Assembly.GetExecutingAssembly.GetName.Name, Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString))

        Application.Run(applicationForm)

        Logger.Instance.Info("Application ended.{0}", ControlChars.CrLf)

    End Sub

End Class

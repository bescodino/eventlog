﻿Imports System.Threading

Public Class MainForm

    Private mServiceThreads As New List(Of Thread)
    Private mExterminatorThread As Thread
    Private mThreadsClosed As Boolean
    Private WithEvents mClosingTimer As New Windows.Forms.Timer
    Private mPower As Boolean = False
    Private mIsConnected As Boolean = False


    Sub New()

        InitializeComponent()
        btnSincronizar.Enabled = False

        MultiLineListBox1 = New MultiLineListBox With {.Dock = DockStyle.Fill}
        Panel1.Controls.Add(MultiLineListBox1)
        Controls.Add(Panel1)

        RefreshPorts()
    End Sub


    Public ReadOnly Property ListBox As MultiLineListBox
        Get
            Return _MultiLineListBox1
        End Get
    End Property


    Private Sub CloseAll()

        If (Not mThreadsClosed) Then

            If (mExterminatorThread Is Nothing) Then

                mExterminatorThread = New Thread(New ThreadStart(AddressOf StopThreads)) With {.Name = "Exterminator"}
                mExterminatorThread.Start()

            End If

        End If

    End Sub


    Private Sub MainForm_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If (Not mThreadsClosed) Then

            e.Cancel = True

            If (mExterminatorThread Is Nothing) Then

                mExterminatorThread = New Thread(New ThreadStart(AddressOf StopThreads)) With {.Name = "Exterminator"}
                mExterminatorThread.Start()

                mClosingTimer.Start()

            End If

        End If

    End Sub

    Private Sub mClosingTimer_Tick(sender As Object, e As EventArgs) Handles mClosingTimer.Tick

        If (Not mExterminatorThread.IsAlive) Then

            mClosingTimer.Stop()
            mClosingTimer = Nothing
            mThreadsClosed = True
            Close()

        End If

    End Sub


    Private Sub StartThreads()

        Logger.Instance.Info("Inicializando...")

        ThreadManager.CloseRequested = False

        mExterminatorThread = Nothing

        mServiceThreads.Clear()

        mServiceThreads.Add(New Thread(New ThreadStart(AddressOf InputService.Run)) With {.Name = "InputService", .CurrentCulture = New System.Globalization.CultureInfo("en-US")})

        For Each serviceThread In mServiceThreads
            serviceThread.Priority = ThreadPriority.Normal
            serviceThread.Start()
        Next

    End Sub


    Private Sub StopThreads()

        ThreadManager.CloseRequested = True

        For Each serviceThread In mServiceThreads
            Logger.Instance.Info("Finalizando...")
            serviceThread.Join()
        Next

    End Sub


    Private Sub btnBaixarEventos_Click(sender As Object, e As EventArgs) Handles btnBaixarEventos.Click

        If (Globals.CommonPort.IsOpen() AndAlso mPower = True) Then
            StartThreads()
        Else
            CloseAll()
        End If

    End Sub


    Private Sub cbxPorta_DropDown(sender As Object, e As EventArgs) Handles cbxPorta.DropDown
        RefreshPorts()
    End Sub


    Private Sub RefreshPorts()

        cbxPorta.Items.Clear()

        Dim ports As String() = IO.Ports.SerialPort.GetPortNames()

        For Each Port In ports
            cbxPorta.Items.Add(Port)
        Next

        cbxPorta.SelectedIndex = 0
    End Sub


    Private Sub btnPower_Click(sender As Object, e As EventArgs) Handles btnPower.Click

        If (mPower = False) Then
            Try
                btnPower.Enabled = False
                If (Not Connect()) Then Throw New Exception()
            Catch ex As Exception
                btnPower.Enabled = True
                mPower = False
                Return
            End Try
            btnPower.Enabled = True
            btnBaixarEventos.Enabled = True
            btnPower.Text = "Desconectar"
            mPower = True
        Else

            Try
                btnPower.Enabled = False
                If (Not Disconnect()) Then Throw New Exception
            Catch ex As Exception
                mPower = True
                btnPower.Enabled = True
                btnBaixarEventos.Enabled = False
                Return
            End Try

            mPower = False
            btnPower.Enabled = True
            btnBaixarEventos.Enabled = False
            btnPower.Text = "Conectar"

        End If
    End Sub


    Private Function Connect() As Boolean

        Logger.Instance.Info("Conectando com hardware...")

        Try
            Globals.CommonPort.Open()

            'Globals.CommonPort.ReadTimeout = 5000

            Logger.Instance.Info("Esperando resposta do hardware...")


            Dim intialString As String = "log de eventos."

            For index = 0 To intialString.Length - 1
                Globals.CommonPort.Write(intialString(index))
                Thread.Sleep(2)
            Next

            Thread.Sleep(50)

            Dim o As Char = ChrW(Globals.CommonPort.ReadChar())
            Dim k As Char = ChrW(Globals.CommonPort.ReadChar())

            If (o = "o" AndAlso k = "k") Then
                Logger.Instance.Info("Conectado com sucesso!")
            Else
                Logger.Instance.Warn("Verifique a COM, outro hardware detectado, código: " & o & k)
                Globals.CommonPort.Close()
                Return False
            End If
            btnSincronizar.Enabled = True

        Catch ex As Exception
            If (Globals.CommonPort IsNot Nothing) Then
                Globals.CommonPort.Close()
            End If
            btnSincronizar.Enabled = False
            Logger.Instance.Error("Error ao se conectar com o hardware, verifique a COM desejada...")
            Logger.Instance.Error(ex.Message)
            Return False
        End Try

        Return True

    End Function

    Private Function Disconnect() As Boolean

        Logger.Instance.Info("Desconectando com hardware...")

        Try
            If (Globals.CommonPort.IsOpen()) Then
                Globals.CommonPort.Close()
                btnBaixarEventos.Enabled = False
                btnPower.Enabled = True
                btnPower.Text = "Conectar"
            End If
        Catch ex As Exception
            Logger.Instance.Error("Não foi possível desconectar, porta:" & Globals.CommonPort.PortName & ".")
            btnSincronizar.Enabled = False
            Logger.Instance.Error(ex.Message)
            Return False
        End Try

        btnSincronizar.Enabled = False
        Logger.Instance.Info("Hardware desconectado com sucesso!")

        Return True

    End Function

    Private Sub cbxPorta_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbxPorta.SelectedValueChanged
        Globals.CommonPort = New IO.Ports.SerialPort(cbxPorta.SelectedItem.ToString(), 115200, IO.Ports.Parity.None, 8, 1)
    End Sub

    Private Sub btnSincronizar_Click(sender As Object, e As EventArgs) Handles btnSincronizar.Click
        SynchronizationService.Run()
    End Sub

End Class
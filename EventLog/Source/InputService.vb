﻿Public Class InputService

    Public Shared Sub Run()

        Dim threadManager As New ThreadManager

        If (ThreadManager.CloseRequested) Then Return

        While True

            Try
                Dim package As New Package
                Dim numberOfEvents As Integer

                If (ThreadManager.CloseRequested) Then Return

                If (Globals.CommonPort.IsOpen() = False) Then
                    Globals.CommonPort.Open()
                End If

                If (Globals.IsConnected = False) Then

                    Globals.CommonPort.Write("eventos.")

                    ' Sleep de meio segundo para o envio do hardware
                    System.Threading.Thread.Sleep(400)

                    Dim first = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")
                    Dim second = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")
                    Dim third = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")
                    Dim four = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")
                    Dim five = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")
                    Dim six = Integer.Parse(Globals.CommonPort.ReadByte()).ToString("X")

                    package.mMacAdress = String.Format("{0}:{1}:{2}:{3}:{4}:{5}",
                                        first,
                                        second,
                                        third,
                                        four,
                                        five,
                                        six)

                    Logger.Instance.Info("Equipamento: " & package.mMacAdress & " identificado com sucesso!")

                    Dim cycle As Byte = Globals.CommonPort.ReadByte()
                    numberOfEvents = (cycle * 255) + Globals.CommonPort.ReadByte()

                    Logger.Instance.Info("Pegando: " & numberOfEvents & " eventos do equipamento: ." & package.mMacAdress)

                    System.Threading.Thread.Sleep(400)

                    Globals.IsConnected = True

                End If

                'começo de pacote
                Dim currentByte As Byte = Globals.CommonPort.ReadByte()

                If (currentByte = 230) Then

                    Dim idx As Integer = 1

                    package.mPackageEvents = New List(Of EventPackage)

                    While (currentByte <> 232)

                        currentByte = Globals.CommonPort.ReadByte()

                        If (currentByte = 230) Then
                            currentByte = Globals.CommonPort.ReadByte()
                        End If

                        While (currentByte <> 231)

                            If (ThreadManager.CloseRequested) Then Return

                            package.mPackageEvents.Add(New EventPackage() With
                            {
                                .mDay = currentByte,
                                .mMonth = Globals.CommonPort.ReadByte(),
                                .mYear = Globals.CommonPort.ReadByte(),
                                .mHour = Globals.CommonPort.ReadByte(),
                                .mMinute = Globals.CommonPort.ReadByte(),
                                .mEventNumber = Globals.CommonPort.ReadByte()
                            })

                            currentByte = Globals.CommonPort.ReadByte()

                            numberOfEvents -= 1

                            Dim time As String = New DateTime(2000 + package.mPackageEvents.Last.mYear, package.mPackageEvents.Last.mMonth, package.mPackageEvents.Last.mDay, package.mPackageEvents.Last.mHour, package.mPackageEvents.Last.mMinute, 0).ToString("dd/MM/yyyy HH:mm:ss.fff")
                            Logger.Instance.Info(String.Format("Evento: {0}, horário: {1}", package.mPackageEvents.Last.mEventNumber, time))

                            idx += 1

                        End While

                        currentByte = Globals.CommonPort.ReadByte()

                        Logger.Instance.Info("Byte: " & currentByte)

                    End While

                End If

                If (numberOfEvents <> package.mPackageEvents.Count AndAlso currentByte <> 232) Then
                    Logger.Instance.Error("Erro no buffer.")
                    Throw New NotImplementedException
                End If

            Catch ex As TimeoutException
                If (ThreadManager.CloseRequested) Then Return
                Logger.Instance.Error("Erro de timeout.")
            Catch ex As Exception
                If (ThreadManager.CloseRequested) Then Return
                Logger.Instance.Error(ex.Message.Trim())
            Finally
                If Globals.CommonPort IsNot Nothing Then Globals.CommonPort.Close()
            End Try

        End While

        If (Not threadManager.WaitForNextCycle) Then Return

    End Sub


End Class
